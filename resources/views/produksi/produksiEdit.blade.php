@extends('../component/index')

@section('css')
    <link href="{{ asset('https://cdn.datatables.net/v/dt/jq-3.6.0/dt-1.11.3/datatables.min.css') }}" rel="stylesheet"
    type="text/css"/>  
@endsection

@section('js')
    {{-- <script src = "{{ asset('assets/demo/default/custom/components/forms/widgets/select2.js') }}" 
    type="text/javascript"></script> --}}
    <script src = "{{ asset('https://cdn.datatables.net/v/dt/jq-3.6.0/dt-1.11.3/datatables.min.js') }}" 
    type="text/javascript"></script>
    {{-- <script src = "{{ asset('assets/demo/default/custom/components/datatables/base/data-local.js') }}" 
    type="text/javascript"></script> --}}
    <script src = "{{ asset('assets/demo/default/custom/components/datatables/base/pagination.js') }}"
    type="text/javascript"></script>
@endsection

@section('content')
<div class="m-grid__item m-grid__item--fluid m-wrapper">
    <div class="m-subheader">
        <div class="d-flex align-items-center">
            <div class="mr-auto">
                <h3 class="m-subheader__title m-subheader__title--separator">
                    {{-- {{ $pagetitle }} --}} Edit Produksi
                </h3>
                {{-- {!! $breadcrumb !!} --}}
            </div>
        </div>
    </div>
    <div class="m-content">

        <div class="m-portlet akses-list">

            <form method="post" action="{{ route('produksiUpdate', ['id' => $edit->id]) }}" 
                  class="form-send m-form m-form--fit m-form--label-align-right" 
                  data-redirect="{{ route('produksiList') }}">

                {{ csrf_field() }}
                <div class="m-portlet__body">
                    <div class="form-group m-form__group">
                        <label>
                            Kode Produksi
                        </label>
                        <input type="text" name="kode_produksi" value="{{ $edit->kode_produksi }}" class="form-control m-input">
                    </div>
                    <div class="form-group m-form__group">
                        <label>
                            Mulai Produksi
                        </label>
                        <input type="date" name="tgl_mulai_produksi" value="{{ $edit->tgl_mulai_produksi }}" class="form-control m-input">
                    </div>
                    <div class="form-group m-form__group">
                        <label>
                            Selesai Produksi
                        </label>
                        <input type="date" name="tgl_selesai_produksi" value="{{ $edit->tgl_selesai_produksi }}" class="form-control m-input">
                    </div>
                    <div class="form-group m-form__group">
                        <label>
                            Pabrik
                        </label>
                        <select name="id_lokasi" class="form-control m-input">
                            @foreach ($lokasi as $row)
                                <option value="{{ $row->id }}">{{ $row->id == $edit->id_lokasi ? 'selected':'' }}>{{ $row->kode_lokasi.' - '.$row->lokasi }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group m-form__group">
                        <label>
                            Catatan
                        </label>
                        <textarea name="catatan" class="form-control m-input">{{ $edit->catatan }}</textarea>
                    </div>
                </div>
                <div class="m-portlet__foot m-portlet__foot--fit">
                    <div class="m-form__actions">
                        <button type="submit" class="btn btn-primary">
                            Perbarui Produksi
                        </button>
                        <a href="{{ route('produksiList') }}" class="btn btn-secondary">
                            Kembali ke Daftar
                        </a>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

@endsection
